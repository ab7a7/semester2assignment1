﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment1;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using ZedGraph;

namespace UnitTestProject1
{
    [TestClass]
    public class FormTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            //DOING THIS HAS CAUSED METHODS TO BE PUBLIC WHICH IS A SECURITY ISSUE
            Form1 a = new Form1();
            a.Show();
            a.clearScreen();
            a.Close();
            a.arrayHR[1] = 5;
            a.arrayHR[5] = 5;
            for (int i = 0; i < a.arrayHR.Length; i++)
            {
                a.arrayHR[i] = a.arrayHR.Length + 6;
            }
        }
    }
}

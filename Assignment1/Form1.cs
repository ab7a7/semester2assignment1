﻿/// <summary>
/// @version 3
/// @author Muhammed Abrar Ali
/// </summary>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using ZedGraph;


namespace Assignment1
{
    public partial class Form1 : Form
    {
        //Variables
        public double[] arrayHR = new double[3];
        public double[] arrayPower = new double[3];
        double[] arrayAlt = new double[3];
        double[] arraySpeed = new double[3];
        double[] arrayCadence = new double[3];
        string[] arrayOfFiles;
        DateTime[] dates;
        public int totalRows = 0;
        int intervals = 1;
        //Distance for distance
        int distance = 0;
        //To Handle Start Time from HRM file
        TimeSpan start = new TimeSpan();
        //Interval time from HRM file
        TimeSpan interval = new TimeSpan();
        double second;
        //Date from HRM file
        DateTime date;
        //Handles if buttons is clicked.
        Boolean calcButton = false, radioChecked = false, checkCalcButton = false;
        //for min, average and max speed.
        double minSpeed = 1000, maxSpeed = -1, averageSpeed = 0;
        //Duration of Race 
        TimeSpan duration = new TimeSpan();
        public Form1()
        {
            InitializeComponent();
            //set Manual averages to false 
            lblManTimeIntervals.Visible = false;
        }

        /// <summary>
        /// <summary>
        /// This is done when the user clicks the Select Date button. The code calls up a dialog then process and displays the HRM file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadFile(string file)
        {
            //Clears Screen
            clearScreen();
            //Enables Calculate and other buttons.
            calcButton = true;
            calculate.Enabled = true;
            clear.Enabled = true;
            btnUpdateRows.Enabled = true;
            //Variables to hold information from HRM file.
            string rowValue;
            string[] cellValue;
            //Stram is created with the HRM file.
            StreamReader Reader1 = new StreamReader(file);
            //FileLine is used to get certain info from the HRM file Used in Header info.
            String fileLine;
            //Used specifically for SMode which then tells code which columns to make.
            String columns = "";
            //End race time.
            TimeSpan end = new TimeSpan();
            //Code which repeats till fileLine is null...in other words no more information in File
            while ((fileLine = Reader1.ReadLine()) != null)
            {
                //Checks when Line starts with Monitor.
                if (fileLine.StartsWith("Monitor="))
                {
                    //Parses the line to drop the useless information.
                    int monitor = int.Parse(fileLine.Replace("Monitor=", ""));
                    header.Text = "Monitor: " + monitor;
                }
                //Checks when line is with SMode.
                if (fileLine.StartsWith("SMode="))
                {
                    columns = fileLine.Replace("SMode=", "");
                }
                //Checks when line is starting with Date. Then parses the date and shows date.
                if (fileLine.StartsWith("Date="))
                {
                    date = DateTime.ParseExact(fileLine.Replace("Date=", ""), "yyyyMMdd", CultureInfo.InvariantCulture);
                    //date = DateTime.ParseExact(fileLine.Replace("Date=", ""), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                    header.Text += Environment.NewLine + "Date: " + date.ToShortDateString();
                }
                //checks when Line Starts with Start Time.
                if (fileLine.StartsWith("StartTime="))
                {
                    start = TimeSpan.Parse(fileLine.Replace("StartTime=", ""));
                    header.Text += Environment.NewLine + "Start Time: " + start;
                }
                //checks when Line Starts with Length. Then Calculates end time and displays them both.
                if (fileLine.StartsWith("Length="))
                {
                    duration = TimeSpan.Parse(fileLine.Replace("Length=", ""));
                    header.Text += Environment.NewLine + "Duration: " + duration;
                    end = start.Add(duration);
                    header.Text += Environment.NewLine + "End Time: " + end;
                }
                //checks when Line Starts with Interval.
                if (fileLine.StartsWith("Interval="))
                {
                    second = double.Parse(fileLine.Replace("Interval=", ""));
                    interval = TimeSpan.FromSeconds(second);
                    header.Text += Environment.NewLine + "Interval Time (s): " + interval;
                }
                //Checks when Trip is found. Then gets the distance travelled as set in the HRM file.
                if (fileLine.Contains("[Trip]"))
                {
                    distance = int.Parse(fileLine.Replace("[Trip]", "") + Reader1.ReadLine()) / 10;
                    distanceLabel.Text = "Distance (KM): " + distance;
                }
                //Drops out of While loop when HRData comes.
                if (fileLine.Contains("[HRData]"))
                {
                    break;
                }
            }
            //SMode Data. Each character represents a row.
            char[] data = columns.ToCharArray();

            //This first character for HR.
            if (data[0].ToString() == "1")
            {
                //Creates Column
                DataGridViewColumn heartrate = new DataGridViewTextBoxColumn();
                heartrate.HeaderText = "Heart Rate";
                heartrate.Name = "heartrate";
                int ColumnIndex2 = dataGridView1.Columns.Add(heartrate);
            }
            //This Character for Speed
            if (data[1].ToString() == "1")
            {
                //Creates Column
                DataGridViewColumn speed = new DataGridViewTextBoxColumn();
                speed.HeaderText = "Speed";
                speed.Name = "speed";
                int ColumnIndex3 = dataGridView1.Columns.Add(speed);
            }
            //This is for Cadence.
            if (data[2].ToString() == "1")
            {
                //Creates Column
                DataGridViewColumn cadence = new DataGridViewTextBoxColumn();
                cadence.HeaderText = "Cadence";
                cadence.Name = "cadence";
                int ColumnIndex4 = dataGridView1.Columns.Add(cadence);
            }
            //This character for Altitude.
            if (data[3].ToString() == "1")
            {
                //Creates Column
                DataGridViewColumn alt = new DataGridViewTextBoxColumn();
                alt.HeaderText = "Altitude";
                alt.Name = "alt";
                int ColumnIndex5 = dataGridView1.Columns.Add(alt);
            }
            //This for power
            if (data[4].ToString() == "1")
            {
                //Creates Column
                DataGridViewColumn power = new DataGridViewTextBoxColumn();
                power.HeaderText = "Power";
                power.Name = "power";
                int ColumnIndex6 = dataGridView1.Columns.Add(power);
            }
            //This for power Balance.
            if (data[5].ToString() == "1")
            {
                //Creates Column
                DataGridViewColumn pwrBal = new DataGridViewTextBoxColumn();
                pwrBal.HeaderText = "Power Balance";
                pwrBal.Name = "pwrBal";
                int ColumnIndex7 = dataGridView1.Columns.Add(pwrBal);
            }
            //Selects a radio button depending on this character.
            if (data[7].ToString() == "0")
            {
                metersRadio.Select();
            }
            else
            {
                milesRadio.Select();
            }

            //Peeks ahead until Peek returns -1 meaning nothing left to peek at
            while (Reader1.Peek() != -1)
            {
                //Ensures a column exists.
                if (dataGridView1.Columns.Contains("heartrate"))
                {
                    //Reads or consumes the line.
                    rowValue = Reader1.ReadLine();
                    //Splits line
                    cellValue = rowValue.Split('\t');
                    //adds data to the GridVIew.
                    dataGridView1.Rows.Add(cellValue);
                }
            }
            //Adds Time to RowHeaderCell.
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.HeaderCell.Value = start.ToString();
                start = start.Add(interval);
            }
            //Cycles through the code and formats the speed correctly.
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                double formatData = double.Parse(dataGridView1.Rows[i].Cells["speed"].Value.ToString());
                dataGridView1.Rows[i].Cells["speed"].Value = formatData / 10;
            }
            //Resizes the Width of headercell so time is viewable.
            dataGridView1.AutoResizeRowHeadersWidth(0, DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
            totalRows = dataGridView1.Rows.Count - 1;
            panelGraphControl.Enabled = true;
            btnIntervalAvg.Enabled = true;
            btnMetrics.Enabled = true;
        }

        /// <summary>
        /// is run when the Calculate button is clicked. Calculates various figures and outputs them to the text boxes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void calculate_Click(object sender, EventArgs e)
        {
            //Ensures button is active
            if (calcButton)
            {
                averageSpeed = 0;
                //Activates Formatting options.
                formatButton.Enabled = true;
                milesRadio.Enabled = true;
                //use4d for average
                double HRsum = 0, spdSum = 0, pwrSum = 0, altSum = 0, cadSum = 0;
                //Variables for the data.
                double minHR = 1000, maxHR = 0, minAlt = 1000, maxAlt = 0, minPower = 1000, maxPower = -1, minCadence = 1000, maxCadence = -1;
                //Cycles through the heartrates and adds them up.
                for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
                {
                    //Data is generic variable used to get data from the GridView.
                    string data = dataGridView1.Rows[i].Cells["heartrate"].Value.ToString();
                    //Calculates Min and Max Heart Rate.
                    minHR = Math.Min(minHR, int.Parse(data));
                    maxHR = Math.Max(maxHR, int.Parse(data));
                    HRsum += Convert.ToDouble(dataGridView1.Rows[i].Cells["heartrate"].Value);
                    data = dataGridView1.Rows[i].Cells["speed"].Value.ToString();
                    //Calculates Min and Max Speed.
                    minSpeed = Math.Min(minSpeed, double.Parse(data));
                    maxSpeed = Math.Max(maxSpeed, double.Parse(data));
                    spdSum += Convert.ToDouble(dataGridView1.Rows[i].Cells["speed"].Value);

                    data = dataGridView1.Rows[i].Cells["power"].Value.ToString();
                    //Calculates Min and Max Power.
                    minPower = Math.Min(minPower, int.Parse(data));
                    maxPower = Math.Max(maxPower, int.Parse(data));
                    pwrSum += Convert.ToDouble(dataGridView1.Rows[i].Cells["power"].Value);

                    data = dataGridView1.Rows[i].Cells["alt"].Value.ToString();
                    //Calculates Min and Max Altitude.
                    minAlt = Math.Min(minAlt, int.Parse(data));
                    maxAlt = Math.Max(maxAlt, int.Parse(data));
                    altSum += Convert.ToDouble(dataGridView1.Rows[i].Cells["alt"].Value);

                    data = dataGridView1.Rows[i].Cells["cadence"].Value.ToString();
                    minCadence = Math.Min(minCadence, int.Parse(data));
                    maxCadence = Math.Max(maxCadence, int.Parse(data));
                    cadSum += Convert.ToDouble(dataGridView1.Rows[i].Cells["cadence"].Value);
                }
                //Finding average by diving the sum with row count.
                double averageHR = HRsum / (dataGridView1.Rows.Count - 1);
                averageSpeed = spdSum / (dataGridView1.Rows.Count - 1);
                double averagePower = (pwrSum / dataGridView1.Rows.Count - 1);
                double averageAlt = (altSum / dataGridView1.Rows.Count - 1);
                double averageCad = (cadSum / dataGridView1.Rows.Count - 1);
                //Rounds up variables
                averageHR = Math.Round(averageHR, 1);
                averageSpeed = Math.Round(averageSpeed, 1);
                averagePower = Math.Round(averagePower, 1);
                averageAlt = Math.Round(averageAlt, 1);
                averageCad = Math.Round(averageCad, 1);

                //Sets the text for the text boxes if the total rows is less than current rows displayed, then show average in Manuals Label
                if (dataGridView1.Rows.Count - 1 < totalRows)
                {
                    lblManTimeIntervals.Visible = true;
                    lblManTimeIntervals.Text += Environment.NewLine + intervals + ") Avg HR: " + averageHR + " Avg Speed: " + averageSpeed + " Avg alt: " + averageAlt + " Avg Power: " + averagePower + " Avg Cadence: " + averageCad;
                    intervals++;
                }
                heartRateText.Text = "Min Heart Rate : " + minHR + Environment.NewLine + "Average Heart Rate: " + averageHR + Environment.NewLine + "Max Heart Rate: " + maxHR;
                speedText.Text = "Min Speed (KM): " + minSpeed + Environment.NewLine + "Average Speed (KM): " + averageSpeed + Environment.NewLine + "Max Speed (KM): " + maxSpeed;
                altText.Text = "Min Altitude: " + minAlt + Environment.NewLine + "Average Altitude: " + averageAlt + Environment.NewLine + "Max Altitude: " + maxAlt;
                powerText.Text = "Min Power: " + minPower + Environment.NewLine + "Average Power: " + averagePower + Environment.NewLine + "Max Power: " + maxPower;
                cadenceText.Text = "Min Cadence: " + minCadence + Environment.NewLine + "Average Cadence: " + averageCad + Environment.NewLine + "Max Cadence: " + maxCadence;

                for (int i = 0; i < 3; i++)
                {
                    if (i == 0)
                    {
                        arrayHR[i] = minHR;
                        arraySpeed[i] = minSpeed;
                        arrayAlt[i] = minAlt;
                        arrayPower[i] = minPower;
                        arrayCadence[i] = minCadence;
                    }
                    if (1 == 1)
                    {
                        arrayHR[i] = averageHR;
                        arraySpeed[i] = averageSpeed;
                        arrayAlt[i] = averageAlt;
                        arrayPower[i] = averagePower;
                        arrayCadence[i] = averageCad;
                    }
                    if (i == 2)
                    {
                        arrayHR[i] = maxHR;
                        arraySpeed[i] = maxSpeed;
                        arrayAlt[i] = maxAlt;
                        arrayPower[i] = maxPower;
                        arrayCadence[i] = maxCadence;
                    }
                }
                //Allows rad btn
                radBtnBar.Enabled = true;
                //Deactivates buttons to ensure code isnt calcuated again unnecessarily.
                calculate.Enabled = false;
                checkCalcButton = true;
            }
        }

        /// <summary>
        /// <para>This code is run when clear button is clicked. It basically resets all the fields by calling the method clearScreen().</para>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void clear_Click(object sender, EventArgs e)
        {
            clearScreen();
        }

        /// <summary>
        /// <para>This code is run when clear button is clicked. It basically resets all the fields.</para>
        /// </summary>
        public void clearScreen()
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            heartRateText.Clear();
            speedText.Clear();
            altText.Clear();
            powerText.Clear();
            cadenceText.Clear();
            header.Text = String.Empty;
            clear.Enabled = false;
            calculate.Enabled = false;
            formatButton.Enabled = false;
            metersRadio.Enabled = false;
            milesRadio.Enabled = false;
            distanceLabel.Text = String.Empty;
            panelGraphControl.Enabled = false;
            clearGraph();
            radBtnBar.Enabled = false;
            btnUpdateRows.Enabled = false;
            btnIntervalAvg.Enabled = false;
            lblTime.Text = "Interval Times";
            lblIntAvg.Text = "Averages";
            btnMetrics.Enabled = false;
            txtBoxMetrics.Clear();
        }

        /// <summary>
        /// This code runs when format button is clicked. It changes the SPeed information from KPH to Miles and back.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void formatButton_Click(object sender, EventArgs e)
        {
            //checks if Calculate button is pressed.
            if (checkCalcButton)
            {
                //Checks which formatting to do
                if (milesRadio.Checked && milesRadio.Enabled)
                {
                    //cycles the dataGrid View and changs the data to Miles.
                    for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                    {
                        double miles = double.Parse(dataGridView1.Rows[i].Cells["speed"].Value.ToString());
                        miles = 0.621371192 * miles;
                        dataGridView1.Rows[i].Cells["speed"].Value = Math.Round(miles, 2);

                    }
                    //Changes various other figures to appropriate format.
                    speedText.Text = "Min Speed (M): " + Math.Round((0.621371192 * minSpeed), 2) + Environment.NewLine + "Avg Speed (M): " + Math.Round((0.621371192 * averageSpeed), 2) + Environment.NewLine + "Max Speed (M): " + Math.Round((0.621371192 * maxSpeed), 2);
                    distanceLabel.Text = "Distance (M): " + Math.Round((0.621371192 * distance), 2);
                    dataGridView1.Columns[1].HeaderText = "Speed (M)";
                    milesRadio.Enabled = false;
                    metersRadio.Enabled = true;
                    radioChecked = true;
                }
                if (metersRadio.Checked && metersRadio.Enabled)
                {
                    if (radioChecked)
                    {
                        for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                        {
                            double miles = double.Parse(dataGridView1.Rows[i].Cells["speed"].Value.ToString());
                            miles = 1.609344 * miles;
                            dataGridView1.Rows[i].Cells["speed"].Value = Math.Round(miles, 2);
                        }
                        //Changes various other figures to appropriate format.
                        distanceLabel.Text = "Distance (KM): " + distance;
                        speedText.Text = "Min Speed (KM): " + minSpeed + Environment.NewLine + "Average Speed (KM): " + averageSpeed + Environment.NewLine + "Max Speed (KM): " + maxSpeed;
                        dataGridView1.Columns[1].HeaderText = "Speed (K)";
                        milesRadio.Enabled = true;
                        metersRadio.Enabled = false;
                        radioChecked = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("Calculate Button First.");
            }
        }
        /// <summary>
        /// Builds the graph and displays
        /// </summary>
        /// <param name="zedGraph"></param>
        public void CreateGraph(ZedGraphControl zedGraph)
        {
            GraphPane dataPane = zedGraph.GraphPane;
            clearGraph();
            if (radBtnLine.Checked)
            {
                dataPane.XAxis.IsVisible = true;
                dataPane.YAxis.IsVisible = true;
                //Graph headings
                dataPane.Title.Text = "Cycling Line Graph";
                dataPane.XAxis.Title.Text = "Time (s)";
                dataPane.YAxis.Title.Text = "";
                dataPane.XAxis.Type = AxisType.Linear;

                double xPosition = 0;
                double heartRate, Speed, cadence, altitude, power;
                xPosition = 0;
                PointPairList lineHR = new PointPairList();
                PointPairList lineSpeed = new PointPairList();
                PointPairList lineCad = new PointPairList();
                PointPairList lineAlt = new PointPairList();
                PointPairList linePower = new PointPairList();

                //Draws graph depending on which checkbox are checked.
                for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
                {
                    if (checkBoxHR.Checked)
                    {
                        heartRate = (Convert.ToDouble(dataGridView1.Rows[i].Cells["heartrate"].Value));
                        lineHR.Add(xPosition, heartRate);
                    }
                    if (checkBoxSpeed.Checked)
                    {
                        Speed = (Convert.ToDouble(dataGridView1.Rows[i].Cells["speed"].Value));
                        lineSpeed.Add(xPosition, Speed);
                    }
                    if (checkBoxCadence.Checked)
                    {
                        cadence = (Convert.ToDouble(dataGridView1.Rows[i].Cells["cadence"].Value));
                        lineCad.Add(xPosition, cadence);
                    }
                    if (checkBoxAlt.Checked)
                    {
                        altitude = (Convert.ToDouble(dataGridView1.Rows[i].Cells["alt"].Value));
                        lineAlt.Add(xPosition, altitude);
                    }
                    if (checkBoxPower.Checked)
                    {
                        power = (Convert.ToDouble(dataGridView1.Rows[i].Cells["power"].Value));
                        linePower.Add(xPosition, power);
                    }
                    xPosition++;
                }
                //Adds the legend
                if (checkBoxHR.Checked)
                {
                    LineItem heartrateLine = dataPane.AddCurve("Heart Rate", lineHR, Color.Black, SymbolType.None);
                }
                if (checkBoxSpeed.Checked)
                {
                    LineItem speedLine = dataPane.AddCurve("speed", lineSpeed, Color.Blue, SymbolType.None);
                }
                if (checkBoxCadence.Checked)
                {
                    LineItem cadenceLine = dataPane.AddCurve("cadence", lineCad, Color.Yellow, SymbolType.None);
                }
                if (checkBoxAlt.Checked)
                {
                    LineItem altLine = dataPane.AddCurve("alitute", lineAlt, Color.Green, SymbolType.None);
                }
                if (checkBoxPower.Checked)
                {
                    LineItem powerLine = dataPane.AddCurve("power", linePower, Color.Red, SymbolType.None);
                }
            }
            else if (radBtnBar.Checked)
            {
                //headings
                dataPane.XAxis.IsVisible = true;
                dataPane.YAxis.IsVisible = true;
                dataPane.Title.Text = "Cycling Bar Graph (Min/Avg/Max)";
                dataPane.XAxis.Title.Text = "Data";
                dataPane.YAxis.Title.Text = "";
                //Labels
                string[] labels = { "Minimum", "Average", "Maximum" };
                BarItem bar;
                if (checkBoxHR.Checked)
                {
                    bar = dataPane.AddBar("Heart Rate", null, arrayHR, Color.Black);
                }
                if (checkBoxSpeed.Checked)
                {
                    bar = dataPane.AddBar("Speed", null, arraySpeed, Color.Blue);
                }
                if (checkBoxCadence.Checked)
                {
                    bar = dataPane.AddBar("Cadence", null, arrayCadence, Color.Yellow);
                }
                if (checkBoxAlt.Checked)
                {
                    bar = dataPane.AddBar("Altitude", null, arrayAlt, Color.Green);
                }
                if (checkBoxPower.Checked)
                {
                    bar = dataPane.AddBar("Power", null, arrayPower, Color.Red);
                }
                // Set the XAxis labels
                dataPane.XAxis.Scale.TextLabels = labels;
                // Set the XAxis to Text type
                dataPane.XAxis.Type = AxisType.Text;
            }
            else if (radBtnPie.Checked)
            {
                //Pie chart rows
                if (dataGridView1.Rows.Count > 5)
                {
                    //Set visibility of axis to false
                    dataPane.XAxis.IsVisible = false;
                    dataPane.YAxis.IsVisible = false;
                    //Graph headings
                    dataPane.Title.Text = "Cycling Bar Graph - HR Zones";
                    //Hr value from datagrid view
                    double HRValue = 0;
                    //User FTP
                    double UserFTP = Convert.ToDouble(FTPNum.Value);
                    //Works out the ranges
                    Double minZone = (UserFTP * 60) / 10;
                    Double lightZone = (UserFTP * 70) / 10;
                    Double modZone = (UserFTP * 80) / 10;
                    Double hardZone = (UserFTP * 90) / 10;
                    Double maxZone = UserFTP * 10;
                    dataPane.Legend.Position = LegendPos.Top;
                    //cycles through and adds 1 to each category depending on what the value is.
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        HRValue += Convert.ToInt32(dataGridView1.Rows[i].Cells["heartrate"].Value);
                        if (minZone <= HRValue)
                        {
                            minZone++;
                        }
                        else if (lightZone <= HRValue)
                        {
                            lightZone++;
                        }
                        else if (modZone <= HRValue)
                        {
                            modZone++;
                        }
                        else if (hardZone <= HRValue)
                        {
                            hardZone++;
                        }
                        else if (maxZone <= HRValue)
                        {
                            maxZone++;
                        }
                    }
                    PieItem segment1 = dataPane.AddPieSlice(minZone, Color.LightBlue, .20, "Very Light Exercise 50-60%");
                    PieItem segment2 = dataPane.AddPieSlice(lightZone, Color.Salmon, 0, "Light Exercise 60-70%");
                    PieItem segment3 = dataPane.AddPieSlice(modZone, Color.Yellow, .0, "Moderate Exercise 70-80%");
                    PieItem segment4 = dataPane.AddPieSlice(hardZone, Color.Orange, 0, "Hard Exercise 80-90%");
                    PieItem segment5 = dataPane.AddPieSlice(maxZone, Color.Red, .3, "Maximum Exercise 90-100%");
                    segment1.LabelType = PieLabelType.Name_Value_Percent;
                    segment2.LabelType = PieLabelType.Name_Value_Percent;
                    segment3.LabelType = PieLabelType.Name_Value_Percent;
                    segment4.LabelType = PieLabelType.Name_Value_Percent;
                    segment5.LabelType = PieLabelType.Name_Value_Percent;
                    CurveList curves = dataPane.CurveList;
                    double total = 0;
                    for (int i = 0; i < curves.Count; i++)
                    {
                        total += ((PieItem)curves[i]).Value;
                    }
                }
                else
                {
                    MessageBox.Show("Please load file");
                }
            }
            //Redraw graph
            zedGraph.AxisChange();
            zedGraph.Invalidate();
            zedGraph.Refresh();
        }
        /// <summary>
        /// When Update button is clicked, this will call Create Graph which will redo the graph.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnGraphUpdate_Click(object sender, EventArgs e)
        {
            //Calls method
            CreateGraph(zedGraphControl1);
        }
        /// <summary>
        /// Clears the Graph.
        /// </summary>
        public void clearGraph()
        {
            zedGraphControl1.GraphPane.CurveList.Clear();
            zedGraphControl1.GraphPane.GraphObjList.Clear();
            zedGraphControl1.Refresh();
        }
        /// <summary>
        /// Gets the user selected rows and redoes the datagridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnUpdateRows_Click(object sender, EventArgs e)
        {
            if (lblIntAvg.Text != "")
            {
                lblIntAvg.Text = "";
                lblTime.Text = "";
            }
            calculate.Enabled = true;
            calcButton = true;
            List<DataGridViewRow> rowCollection = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                rowCollection.Add(dataGridView1.Rows[row.Index]);
            }
            dataGridView1.Rows.Clear();

            foreach (DataGridViewRow row in rowCollection)
            {
                dataGridView1.Rows.Add(row);
            }
        }
        /// <summary>
        /// WHen interval button is clicked this method is sued, which automatically detects the interval and calculates averages.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnIntervalAvg_Click(object sender, EventArgs e)
        {
            lblManTimeIntervals.Text = "Manual Time Average";
            lblManTimeIntervals.Visible = false;
            int pwrDropEnd = 100;
            int pwrDiff = 0;
            List<int> strIntervalTime = new List<int>();
            List<int> endIntervalTime = new List<int>();
            //Gets the intervals from entire datagridview
            for (int i = 3; i < dataGridView1.Rows.Count - 2; i++)
            {
                //get start position
                if (int.Parse(dataGridView1.Rows[i - 1].Cells["power"].Value.ToString()) > int.Parse(dataGridView1.Rows[i].Cells["power"].Value.ToString()) + pwrDropEnd)
                {
                    strIntervalTime.Add(i);
                    //find end
                    for (int y = i; y < dataGridView1.Rows.Count - 2; y++)
                    {
                        pwrDiff = int.Parse(dataGridView1.Rows[y].Cells["power"].Value.ToString()) - int.Parse(dataGridView1.Rows[y + 1].Cells["power"].Value.ToString());
                        if (pwrDiff > pwrDropEnd)
                        {
                            endIntervalTime.Add(y);
                            break;
                        }
                    }
                }
            }
            int numOfIntervals = strIntervalTime.Count;
            //Get the rows for start and end of intervals
            List<DataGridViewRow> startRows = new List<DataGridViewRow>();
            List<DataGridViewRow> endTimeRows = new List<DataGridViewRow>();
            for (int i = 0; i < strIntervalTime.Count - 1; i++)
            {
                startRows.Add(dataGridView1.Rows[strIntervalTime[i]]);
                endTimeRows.Add(dataGridView1.Rows[endIntervalTime[i]]);
            }

            //Output Times
            for (int i = 0; i < startRows.Count; i++)
            {
                lblTime.Text = lblTime.Text + Environment.NewLine + "#" + i + " " + startRows[i].HeaderCell.Value + ", " + endTimeRows[i].HeaderCell.Value;
            }

            int intervalPower = 0;
            int heartRate = 0;
            int numOfRows = 0;
            int speed = 0;
            bool check = false;
            for (int i = 0; i < dataGridView1.Rows.Count - 2; i++)
            {
                //Cycles through and adds up the row information
                for (int x = 0; x < startRows.Count; x++)
                {
                    if (dataGridView1.Rows[i].Index >= startRows[0].Index && dataGridView1.Rows[i].Index <= endTimeRows[0].Index)
                    {
                        intervalPower += Convert.ToInt32(dataGridView1.Rows[i].Cells["power"].Value);
                        heartRate += Convert.ToInt32(dataGridView1.Rows[i].Cells["heartrate"].Value);
                        speed += Convert.ToInt32(dataGridView1.Rows[i].Cells["speed"].Value);
                        numOfRows++;
                    }
                    //Checks if we are past an interval
                    if (i == endTimeRows[0].Index && endTimeRows.Count != 0)
                    {
                        check = true;
                    }
                }

                //If past an interval it will do this calculation and then displays.
                if (check)
                {
                    double averageInterval = intervalPower / numOfRows;
                    double averageIntHR = heartRate / numOfRows;
                    double averageIntSpeed = speed / numOfRows;
                    //MessageBox.Show(intervalPower.ToString() + " " + numOfRows);
                    lblIntAvg.Text = lblIntAvg.Text + Environment.NewLine + "Power:" + averageInterval + "    HR:" + averageIntHR + "    Speed:" + averageIntSpeed;
                    //Remove rows
                    startRows.RemoveAt(0);
                    endTimeRows.RemoveAt(0);
                    //Empty out variables
                    check = false;
                    numOfRows = 0;
                    speed = 0;
                    heartRate = 0;
                    intervalPower = 0;
                }
            }
        }
        /// <summary>
        /// Zoom event for Zed Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oldState"></param>
        /// <param name="newState"></param>
        public void zedGraphControl1_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            GraphPane myPane = sender.GraphPane;
            //Ensures FTP is set
            if (FTPNum.Value != 0 && checkCalcButton == true)
            {
                //Sets default color
                lblFTP.ForeColor = default(Color);
                //Get value from user
                double userFTP = double.Parse(FTPNum.Value.ToString()) / 100;
                int selectedRange;
                int xMin;
                int xMax;
                double avgPower = 0;
                //myPane.CurveList.Remove(minH);
                xMin = Convert.ToInt32(zedGraphControl1.GraphPane.XAxis.Scale.Min);
                xMax = (int)Math.Floor(zedGraphControl1.GraphPane.XAxis.Scale.Max);
                if (xMin < 0)
                {
                    xMin = 0;
                }
                xMin = xMin / int.Parse(second.ToString());
                xMax = xMax / int.Parse(second.ToString());
                selectedRange = xMax - xMin;
                try
                {
                    if (xMax > dataGridView1.Rows.Count)
                    {
                        xMax = dataGridView1.Rows.Count;
                    }
                    for (int i = xMin; i < xMax; i++)
                    {
                        avgPower += Convert.ToDouble(dataGridView1.Rows[i].Cells["power"].Value);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error" + e);
                }

                avgPower = avgPower / selectedRange;


                //POWER ZONES
                lblCoggan.Text += Environment.NewLine + "Power Zone: ";
                //http://home.trainingpeaks.com/blog/article/power-training-levels
                if (avgPower <= (arrayPower[2] * 0.55))
                {
                    lblCoggan.Text += "1 - Active Recovery \nAvg Power: " + avgPower;
                }
                else if (avgPower >= (arrayPower[2] * 0.56) && avgPower <= (arrayPower[2] * 0.75))
                {
                    lblCoggan.Text += "2 - Endurance \nAvg Power: " + avgPower;
                }
                else if (avgPower >= (arrayPower[2] * 0.76) && avgPower <= (arrayPower[2] * 0.9))
                {
                    lblCoggan.Text += "3 - Tempo \nAvg Power: " + avgPower;
                }
                else if (avgPower >= (arrayPower[2] * 0.91) && avgPower <= (arrayPower[2] * 1.05))
                {
                    lblCoggan.Text += "4 - Lactate Threshold \nAvg Power: " + avgPower;
                }
                else if (avgPower >= (arrayPower[2] * 1.06))
                {
                    lblCoggan.Text += "5 - V02 Max \nAvg Power: " + avgPower;
                }
            }
            else
            {
                clearGraph();
                CreateGraph(zedGraphControl1);
                MessageBox.Show("Please Enter your FTP and click Calculate");
                lblFTP.ForeColor = Color.Red;
            }
        }
        /// <summary>
        /// Metrics button calculation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnMetrics_Click(object sender, EventArgs e)
        {
            //Checks if FTP number is empty
            if (FTPNum.Value != 0)
            {
                //Sets color
                lblFTP.ForeColor = default(Color);
                //http://forum.slowtwitch.com/Slowtwitch_Forums_C1/Triathlon_Forum_F1/Normalized_Power_Formula_or_Calculator..._P3097774/
                List<double> secondBlocks = new List<double>();
                List<double> averageBlock = new List<double>();
                //Get the rolling 30second total
                int blocks = dataGridView1.Rows.Count / 30;
                int increment = 1;
                for (int i = 0; i < blocks; i++)
                {
                    for (int y = 1; y < 31; y++)
                    {
                        secondBlocks.Add(Convert.ToDouble(dataGridView1.Rows[increment].Cells["power"].Value));
                        increment++;
                    }
                    averageBlock.Add(secondBlocks.Average());
                    secondBlocks.Clear();
                }
                //Raise all values in average block to power of 4
                for (int i = 0; i < averageBlock.Count; i++)
                {
                    averageBlock[i] = Math.Pow(averageBlock[i], 4);
                }
                //Getting the 4th root of the average of the average block
                double normPower = Math.Pow(averageBlock.Average(), 1.0 / 4);

                //FTP value from user
                double FunctionalThresPower = double.Parse(FTPNum.Value.ToString());
                //intensity factor
                double iFactor = Math.Round(normPower / FunctionalThresPower, 2);
                //Length of race in seconds
                double seconds = duration.TotalSeconds;
                //https://www.britishcycling.org.uk/insightzone/physical_preparation/planning_for_performance/article/izn20130325-Quantifying-your-Training-Workload-with-Training-Stress-Score-0
                double stressScore = (seconds * normPower * iFactor) / (FunctionalThresPower * 3600) * 100;
                txtBoxMetrics.Text = "NP: " + normPower + Environment.NewLine + "IF: " + iFactor + Environment.NewLine + "TSS: " + stressScore;
            }
            else
            {
                MessageBox.Show("Please enter your FTP");
                lblFTP.ForeColor = Color.Red;
            }
        }
        /// <summary>
        /// Gets an array of files from the users selected folder and then outputs the dates as bold dates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetFiles_Click(object sender, EventArgs e)
        {
            //FolderBrowser
            FolderBrowserDialog FolderBrowser = new FolderBrowserDialog();
            //Shows for user to select
            DialogResult result = FolderBrowser.ShowDialog();
            //Makes sure OK is clicked
            if (result == DialogResult.OK)
            {
                //Builds up an array of files with HRM file ending
                arrayOfFiles = Directory.GetFiles(FolderBrowser.SelectedPath, "*.hrm");
                //dates array based on the number of files
                dates = new DateTime[arrayOfFiles.Length];
                //used to loop through dates
                int i = 0;
                //Loops through all the files till dates are found and adds to dates array
                foreach (string file in arrayOfFiles)
                {
                    StreamReader Reader1 = new StreamReader(file);
                    string fileLine;

                    while ((fileLine = Reader1.ReadLine()) != null)
                    {
                        //Checks when line is starting with Date. Then parses the date and shows date.
                        if (fileLine.StartsWith("Date="))
                        {
                            dates[i] = DateTime.ParseExact(fileLine.Replace("Date=", ""), "yyyyMMdd", CultureInfo.InvariantCulture);
                            i++;
                            break;
                        }
                    }
                }
                //adds date to calender as bold date
                monthCalendar1.BoldedDates = dates;
            }
        }
        /// <summary>
        /// Scans the files and does so till date selected is found then loads it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDateSelect_Click(object sender, EventArgs e)
        {
            //checks if date is bold
            if (monthCalendar1.BoldedDates.Contains(monthCalendar1.SelectionStart))
            {
                //Bool used for file found
                bool fileFound = true;
                ///Loops through all the files till right one with date matching is found then stops
                foreach (string file in arrayOfFiles)
                {
                    DateTime fileDate;
                    //Reader for the file 
                    StreamReader Reader1 = new StreamReader(file);
                    //For file line
                    string fileLine;
                    //checks if file is found
                    if (fileFound)
                    {
                        //carries on till reader is out of lines
                        while ((fileLine = Reader1.ReadLine()) != null)
                        {
                            //Checks when line is starting with Date. Then parses the date and shows date.
                            if (fileLine.StartsWith("Date="))
                            {
                                //get date from file
                                fileDate = DateTime.ParseExact(fileLine.Replace("Date=", ""), "yyyyMMdd", CultureInfo.InvariantCulture);
                                //compare the dates
                                if (fileDate == monthCalendar1.SelectionStart)
                                {

                                    dataGridView1.Rows.Clear();
                                    loadFile(file);
                                    fileFound = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select Bold date");
            }
        }
    }
}

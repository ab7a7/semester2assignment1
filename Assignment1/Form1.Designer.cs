﻿namespace Assignment1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.calculate = new System.Windows.Forms.Button();
            this.heartRateText = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.header = new System.Windows.Forms.Label();
            this.clear = new System.Windows.Forms.Button();
            this.speedText = new System.Windows.Forms.TextBox();
            this.altText = new System.Windows.Forms.TextBox();
            this.powerText = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.formatButton = new System.Windows.Forms.Button();
            this.metersRadio = new System.Windows.Forms.RadioButton();
            this.milesRadio = new System.Windows.Forms.RadioButton();
            this.distanceLabel = new System.Windows.Forms.Label();
            this.cadenceText = new System.Windows.Forms.TextBox();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.panelGraphControl = new System.Windows.Forms.Panel();
            this.radBtnPie = new System.Windows.Forms.RadioButton();
            this.radBtnBar = new System.Windows.Forms.RadioButton();
            this.radBtnLine = new System.Windows.Forms.RadioButton();
            this.btnGraphUpdate = new System.Windows.Forms.Button();
            this.checkBoxPower = new System.Windows.Forms.CheckBox();
            this.checkBoxAlt = new System.Windows.Forms.CheckBox();
            this.checkBoxCadence = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxHR = new System.Windows.Forms.CheckBox();
            this.btnUpdateRows = new System.Windows.Forms.Button();
            this.btnIntervalAvg = new System.Windows.Forms.Button();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblIntAvg = new System.Windows.Forms.Label();
            this.txtBoxMetrics = new System.Windows.Forms.TextBox();
            this.FTPNum = new System.Windows.Forms.NumericUpDown();
            this.lblFTP = new System.Windows.Forms.Label();
            this.btnMetrics = new System.Windows.Forms.Button();
            this.lblCoggan = new System.Windows.Forms.Label();
            this.lblManTimeIntervals = new System.Windows.Forms.Label();
            this.calenderPanel = new System.Windows.Forms.Panel();
            this.btnDateSelect = new System.Windows.Forms.Button();
            this.btnGetFiles = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panelGraphControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FTPNum)).BeginInit();
            this.calenderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(5, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(749, 285);
            this.dataGridView1.TabIndex = 2;
            // 
            // calculate
            // 
            this.calculate.Enabled = false;
            this.calculate.Location = new System.Drawing.Point(12, 322);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(97, 26);
            this.calculate.Location = new System.Drawing.Point(115, 323);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(94, 24);
            this.calculate.TabIndex = 3;
            this.calculate.Text = "Calculate";
            this.calculate.UseVisualStyleBackColor = true;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // heartRateText
            // 
            this.heartRateText.Location = new System.Drawing.Point(5, 374);
            this.heartRateText.Name = "heartRateText";
            this.heartRateText.Size = new System.Drawing.Size(142, 96);
            this.heartRateText.TabIndex = 4;
            this.heartRateText.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 477);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Header Information";
            // 
            // header
            // 
            this.header.AutoSize = true;
            this.header.Location = new System.Drawing.Point(12, 500);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(0, 13);
            this.header.TabIndex = 6;
            // 
            // clear
            // 
            this.clear.Enabled = false;
            this.clear.Location = new System.Drawing.Point(123, 322);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(97, 26);
            this.clear.Location = new System.Drawing.Point(215, 323);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(97, 24);
            this.clear.TabIndex = 7;
            this.clear.Text = "Clear";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // speedText
            // 
            this.speedText.Location = new System.Drawing.Point(160, 374);
            this.speedText.Multiline = true;
            this.speedText.Name = "speedText";
            this.speedText.Size = new System.Drawing.Size(142, 96);
            this.speedText.TabIndex = 8;
            // 
            // altText
            // 
            this.altText.Location = new System.Drawing.Point(455, 374);
            this.altText.Multiline = true;
            this.altText.Name = "altText";
            this.altText.Size = new System.Drawing.Size(142, 96);
            this.altText.TabIndex = 9;
            // 
            // powerText
            // 
            this.powerText.Location = new System.Drawing.Point(603, 374);
            this.powerText.Multiline = true;
            this.powerText.Name = "powerText";
            this.powerText.Size = new System.Drawing.Size(142, 96);
            this.powerText.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.formatButton);
            this.groupBox1.Controls.Add(this.metersRadio);
            this.groupBox1.Controls.Add(this.milesRadio);
            this.groupBox1.Location = new System.Drawing.Point(585, 303);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(160, 65);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Format";
            // 
            // formatButton
            // 
            this.formatButton.Enabled = false;
            this.formatButton.Location = new System.Drawing.Point(57, 29);
            this.formatButton.Name = "formatButton";
            this.formatButton.Size = new System.Drawing.Size(97, 26);
            this.formatButton.TabIndex = 2;
            this.formatButton.Text = "Format!";
            this.formatButton.UseVisualStyleBackColor = true;
            this.formatButton.Click += new System.EventHandler(this.formatButton_Click);
            // 
            // metersRadio
            // 
            this.metersRadio.AutoSize = true;
            this.metersRadio.Enabled = false;
            this.metersRadio.Location = new System.Drawing.Point(7, 44);
            this.metersRadio.Name = "metersRadio";
            this.metersRadio.Size = new System.Drawing.Size(47, 17);
            this.metersRadio.TabIndex = 1;
            this.metersRadio.TabStop = true;
            this.metersRadio.Text = "KPH";
            this.metersRadio.UseVisualStyleBackColor = true;
            // 
            // milesRadio
            // 
            this.milesRadio.AutoSize = true;
            this.milesRadio.Enabled = false;
            this.milesRadio.Location = new System.Drawing.Point(7, 20);
            this.milesRadio.Name = "milesRadio";
            this.milesRadio.Size = new System.Drawing.Size(49, 17);
            this.milesRadio.TabIndex = 0;
            this.milesRadio.TabStop = true;
            this.milesRadio.Text = "MPH";
            this.milesRadio.UseVisualStyleBackColor = true;
            // 
            // distanceLabel
            // 
            this.distanceLabel.AutoSize = true;
            this.distanceLabel.Location = new System.Drawing.Point(775, 329);
            this.distanceLabel.Name = "distanceLabel";
            this.distanceLabel.Size = new System.Drawing.Size(0, 13);
            this.distanceLabel.TabIndex = 13;
            // 
            // cadenceText
            // 
            this.cadenceText.Location = new System.Drawing.Point(308, 374);
            this.cadenceText.Multiline = true;
            this.cadenceText.Name = "cadenceText";
            this.cadenceText.Size = new System.Drawing.Size(142, 96);
            this.cadenceText.TabIndex = 14;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(778, 12);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(737, 285);
            this.zedGraphControl1.TabIndex = 15;
            this.zedGraphControl1.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.zedGraphControl1_ZoomEvent);
            // 
            // panelGraphControl
            // 
            this.panelGraphControl.Controls.Add(this.radBtnPie);
            this.panelGraphControl.Controls.Add(this.radBtnBar);
            this.panelGraphControl.Controls.Add(this.radBtnLine);
            this.panelGraphControl.Controls.Add(this.btnGraphUpdate);
            this.panelGraphControl.Controls.Add(this.checkBoxPower);
            this.panelGraphControl.Controls.Add(this.checkBoxAlt);
            this.panelGraphControl.Controls.Add(this.checkBoxCadence);
            this.panelGraphControl.Controls.Add(this.checkBoxSpeed);
            this.panelGraphControl.Controls.Add(this.checkBoxHR);
            this.panelGraphControl.Enabled = false;
            this.panelGraphControl.Location = new System.Drawing.Point(781, 303);
            this.panelGraphControl.Name = "panelGraphControl";
            this.panelGraphControl.Size = new System.Drawing.Size(734, 65);
            this.panelGraphControl.TabIndex = 16;
            // 
            // radBtnPie
            // 
            this.radBtnPie.AutoSize = true;
            this.radBtnPie.Location = new System.Drawing.Point(457, 45);
            this.radBtnPie.Name = "radBtnPie";
            this.radBtnPie.Size = new System.Drawing.Size(40, 17);
            this.radBtnPie.TabIndex = 3;
            this.radBtnPie.TabStop = true;
            this.radBtnPie.Text = "Pie";
            this.radBtnPie.UseVisualStyleBackColor = true;
            // 
            // radBtnBar
            // 
            this.radBtnBar.AutoSize = true;
            this.radBtnBar.Enabled = false;
            this.radBtnBar.Location = new System.Drawing.Point(457, 24);
            this.radBtnBar.Location = new System.Drawing.Point(457, 44);
            this.radBtnBar.Name = "radBtnBar";
            this.radBtnBar.Size = new System.Drawing.Size(41, 17);
            this.radBtnBar.TabIndex = 3;
            this.radBtnBar.TabStop = true;
            this.radBtnBar.Text = "Bar";
            this.radBtnBar.UseVisualStyleBackColor = true;
            // 
            // radBtnLine
            // 
            this.radBtnLine.AutoSize = true;
            this.radBtnLine.Location = new System.Drawing.Point(457, 3);
            this.radBtnLine.Location = new System.Drawing.Point(457, 21);
            this.radBtnLine.Name = "radBtnLine";
            this.radBtnLine.Size = new System.Drawing.Size(45, 17);
            this.radBtnLine.TabIndex = 3;
            this.radBtnLine.TabStop = true;
            this.radBtnLine.Text = "Line";
            this.radBtnLine.UseVisualStyleBackColor = true;
            // 
            // btnGraphUpdate
            // 
            this.btnGraphUpdate.Location = new System.Drawing.Point(522, 16);
            this.btnGraphUpdate.Name = "btnGraphUpdate";
            this.btnGraphUpdate.Size = new System.Drawing.Size(97, 26);
            this.btnGraphUpdate.Location = new System.Drawing.Point(508, 21);
            this.btnGraphUpdate.Name = "btnGraphUpdate";
            this.btnGraphUpdate.Size = new System.Drawing.Size(81, 33);
            this.btnGraphUpdate.TabIndex = 1;
            this.btnGraphUpdate.Text = "Update";
            this.btnGraphUpdate.UseVisualStyleBackColor = true;
            this.btnGraphUpdate.Click += new System.EventHandler(this.btnGraphUpdate_Click);
            // 
            // checkBoxPower
            // 
            this.checkBoxPower.AutoSize = true;
            this.checkBoxPower.Location = new System.Drawing.Point(354, 24);
            this.checkBoxPower.Location = new System.Drawing.Point(355, 30);
            this.checkBoxPower.Name = "checkBoxPower";
            this.checkBoxPower.Size = new System.Drawing.Size(56, 17);
            this.checkBoxPower.TabIndex = 0;
            this.checkBoxPower.Text = "Power";
            this.checkBoxPower.UseVisualStyleBackColor = true;
            // 
            // checkBoxAlt
            // 
            this.checkBoxAlt.AutoSize = true;
            this.checkBoxAlt.Location = new System.Drawing.Point(276, 24);
            this.checkBoxAlt.Location = new System.Drawing.Point(277, 30);
            this.checkBoxAlt.Name = "checkBoxAlt";
            this.checkBoxAlt.Size = new System.Drawing.Size(61, 17);
            this.checkBoxAlt.TabIndex = 0;
            this.checkBoxAlt.Text = "Altitude";
            this.checkBoxAlt.UseVisualStyleBackColor = true;
            // 
            // checkBoxCadence
            // 
            this.checkBoxCadence.AutoSize = true;
            this.checkBoxCadence.Location = new System.Drawing.Point(201, 24);
            this.checkBoxCadence.Location = new System.Drawing.Point(202, 30);
            this.checkBoxCadence.Name = "checkBoxCadence";
            this.checkBoxCadence.Size = new System.Drawing.Size(69, 17);
            this.checkBoxCadence.TabIndex = 0;
            this.checkBoxCadence.Text = "Cadence";
            this.checkBoxCadence.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed
            // 
            this.checkBoxSpeed.AutoSize = true;
            this.checkBoxSpeed.Location = new System.Drawing.Point(117, 24);
            this.checkBoxSpeed.Location = new System.Drawing.Point(118, 30);
            this.checkBoxSpeed.Name = "checkBoxSpeed";
            this.checkBoxSpeed.Size = new System.Drawing.Size(57, 17);
            this.checkBoxSpeed.TabIndex = 0;
            this.checkBoxSpeed.Text = "Speed";
            this.checkBoxSpeed.UseVisualStyleBackColor = true;
            // 
            // checkBoxHR
            // 
            this.checkBoxHR.AutoSize = true;
            this.checkBoxHR.Location = new System.Drawing.Point(33, 24);
            this.checkBoxHR.Location = new System.Drawing.Point(34, 30);
            this.checkBoxHR.Name = "checkBoxHR";
            this.checkBoxHR.Size = new System.Drawing.Size(78, 17);
            this.checkBoxHR.TabIndex = 0;
            this.checkBoxHR.Text = "Heart Rate";
            this.checkBoxHR.UseVisualStyleBackColor = true;
            // 
            // btnUpdateRows
            // 
            this.btnUpdateRows.Enabled = false;
            this.btnUpdateRows.Location = new System.Drawing.Point(236, 323);
            this.btnUpdateRows.Location = new System.Drawing.Point(318, 323);
            this.btnUpdateRows.Name = "btnUpdateRows";
            this.btnUpdateRows.Size = new System.Drawing.Size(97, 25);
            this.btnUpdateRows.TabIndex = 7;
            this.btnUpdateRows.Text = "Update Rows";
            this.btnUpdateRows.UseVisualStyleBackColor = true;
            this.btnUpdateRows.Click += new System.EventHandler(this.btnUpdateRows_Click);
            // 
            // btnIntervalAvg
            // 
            this.btnIntervalAvg.Enabled = false;
            this.btnIntervalAvg.Location = new System.Drawing.Point(350, 322);
            this.btnIntervalAvg.Location = new System.Drawing.Point(421, 323);
            this.btnIntervalAvg.Name = "btnIntervalAvg";
            this.btnIntervalAvg.Size = new System.Drawing.Size(97, 25);
            this.btnIntervalAvg.TabIndex = 7;
            this.btnIntervalAvg.Text = "Interval Average";
            this.btnIntervalAvg.UseVisualStyleBackColor = true;
            this.btnIntervalAvg.Click += new System.EventHandler(this.btnIntervalAvg_Click);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(778, 374);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(73, 13);
            this.lblTime.TabIndex = 17;
            this.lblTime.Text = "Interval Times";
            // 
            // lblIntAvg
            // 
            this.lblIntAvg.AutoSize = true;
            this.lblIntAvg.Location = new System.Drawing.Point(980, 374);
            this.lblIntAvg.Name = "lblIntAvg";
            this.lblIntAvg.Size = new System.Drawing.Size(85, 13);
            this.lblIntAvg.TabIndex = 17;
            this.lblIntAvg.Text = "Interval Average";
            // 
            // txtBoxMetrics
            // 
            this.txtBoxMetrics.Location = new System.Drawing.Point(455, 474);
            this.txtBoxMetrics.Multiline = true;
            this.txtBoxMetrics.Name = "txtBoxMetrics";
            this.txtBoxMetrics.Size = new System.Drawing.Size(142, 96);
            this.txtBoxMetrics.TabIndex = 10;
            // 
            // FTPNum
            // 
            this.FTPNum.Location = new System.Drawing.Point(329, 550);
            this.FTPNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.FTPNum.Name = "FTPNum";
            this.FTPNum.Size = new System.Drawing.Size(120, 20);
            this.FTPNum.TabIndex = 19;
            // 
            // lblFTP
            // 
            this.lblFTP.AutoSize = true;
            this.lblFTP.Location = new System.Drawing.Point(296, 552);
            this.lblFTP.Name = "lblFTP";
            this.lblFTP.Size = new System.Drawing.Size(27, 13);
            this.lblFTP.TabIndex = 20;
            this.lblFTP.Text = "FTP";
            // 
            // btnMetrics
            // 
            this.btnMetrics.Enabled = false;
            this.btnMetrics.Location = new System.Drawing.Point(603, 545);
            this.btnMetrics.Name = "btnMetrics";
            this.btnMetrics.Size = new System.Drawing.Size(97, 25);
            this.btnMetrics.TabIndex = 7;
            this.btnMetrics.Text = "Metrics";
            this.btnMetrics.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnMetrics.UseVisualStyleBackColor = true;
            this.btnMetrics.Click += new System.EventHandler(this.btnMetrics_Click);
            // 
            // lblCoggan
            // 
            this.lblCoggan.AutoSize = true;
            this.lblCoggan.Location = new System.Drawing.Point(1285, 377);
            this.lblCoggan.Name = "lblCoggan";
            this.lblCoggan.Size = new System.Drawing.Size(124, 13);
            this.lblCoggan.TabIndex = 17;
            this.lblCoggan.Text = "Zoom to DIsplay Coggan";
            // 
            // lblManTimeIntervals
            // 
            this.lblManTimeIntervals.AutoSize = true;
            this.lblManTimeIntervals.Location = new System.Drawing.Point(778, 407);
            this.lblManTimeIntervals.Name = "lblManTimeIntervals";
            this.lblManTimeIntervals.Size = new System.Drawing.Size(111, 13);
            this.lblManTimeIntervals.TabIndex = 21;
            this.lblManTimeIntervals.Text = "Manual Time Average";
            // 
            // calenderPanel
            // 
            this.calenderPanel.Controls.Add(this.btnDateSelect);
            this.calenderPanel.Controls.Add(this.btnGetFiles);
            this.calenderPanel.Controls.Add(this.monthCalendar1);
            this.calenderPanel.Location = new System.Drawing.Point(1213, 428);
            this.calenderPanel.Name = "calenderPanel";
            this.calenderPanel.Size = new System.Drawing.Size(310, 165);
            this.calenderPanel.TabIndex = 22;
            // 
            // btnDateSelect
            // 
            this.btnDateSelect.Location = new System.Drawing.Point(229, 52);
            this.btnDateSelect.Name = "btnDateSelect";
            this.btnDateSelect.Size = new System.Drawing.Size(75, 23);
            this.btnDateSelect.TabIndex = 1;
            this.btnDateSelect.Text = "Date Select";
            this.btnDateSelect.UseVisualStyleBackColor = true;
            this.btnDateSelect.Click += new System.EventHandler(this.btnDateSelect_Click);
            // 
            // btnGetFiles
            // 
            this.btnGetFiles.Location = new System.Drawing.Point(229, 23);
            this.btnGetFiles.Name = "btnGetFiles";
            this.btnGetFiles.Size = new System.Drawing.Size(75, 23);
            this.btnGetFiles.TabIndex = 1;
            this.btnGetFiles.Text = "Get Files";
            this.btnGetFiles.UseVisualStyleBackColor = true;
            this.btnGetFiles.Click += new System.EventHandler(this.btnGetFiles_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(0, 1);
            this.monthCalendar1.MaxSelectionCount = 1;
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1535, 647);
            this.Controls.Add(this.calenderPanel);
            this.Controls.Add(this.lblManTimeIntervals);
            this.Controls.Add(this.lblFTP);
            this.Controls.Add(this.FTPNum);
            this.Controls.Add(this.lblCoggan);
            this.Controls.Add(this.lblIntAvg);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.panelGraphControl);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.cadenceText);
            this.Controls.Add(this.distanceLabel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtBoxMetrics);
            this.Controls.Add(this.powerText);
            this.Controls.Add(this.altText);
            this.Controls.Add(this.speedText);
            this.Controls.Add(this.btnMetrics);
            this.Controls.Add(this.btnIntervalAvg);
            this.Controls.Add(this.btnUpdateRows);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.header);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.heartRateText);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelGraphControl.ResumeLayout(false);
            this.panelGraphControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FTPNum)).EndInit();
            this.calenderPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button calculate;
        private System.Windows.Forms.RichTextBox heartRateText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label header;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.TextBox speedText;
        private System.Windows.Forms.TextBox altText;
        private System.Windows.Forms.TextBox powerText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button formatButton;
        private System.Windows.Forms.RadioButton metersRadio;
        private System.Windows.Forms.RadioButton milesRadio;
        private System.Windows.Forms.Label distanceLabel;
        private System.Windows.Forms.TextBox cadenceText;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Panel panelGraphControl;
        private System.Windows.Forms.Button btnGraphUpdate;
        private System.Windows.Forms.CheckBox checkBoxPower;
        private System.Windows.Forms.CheckBox checkBoxAlt;
        private System.Windows.Forms.CheckBox checkBoxCadence;
        private System.Windows.Forms.CheckBox checkBoxHR;
        private System.Windows.Forms.CheckBox checkBoxSpeed;
        private System.Windows.Forms.RadioButton radBtnBar;
        private System.Windows.Forms.RadioButton radBtnLine;
        private System.Windows.Forms.Button btnUpdateRows;
        private System.Windows.Forms.Button btnIntervalAvg;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblIntAvg;
        private System.Windows.Forms.TextBox txtBoxMetrics;
        private System.Windows.Forms.NumericUpDown FTPNum;
        private System.Windows.Forms.Label lblFTP;
        private System.Windows.Forms.Button btnMetrics;
        private System.Windows.Forms.Label lblCoggan;
        private System.Windows.Forms.RadioButton radBtnPie;
        private System.Windows.Forms.Label lblManTimeIntervals;
        private System.Windows.Forms.Panel calenderPanel;
        private System.Windows.Forms.Button btnDateSelect;
        private System.Windows.Forms.Button btnGetFiles;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
    }
}

